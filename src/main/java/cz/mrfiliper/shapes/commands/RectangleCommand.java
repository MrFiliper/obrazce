package cz.mrfiliper.shapes.commands;

import cz.mrfiliper.shapes.shapes.Rectangle;
import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Class represents rectangle command 'obdelnik'
 */
public class RectangleCommand implements CommandInterface {

    private List<ShapeInterface> list;

    public RectangleCommand(List<ShapeInterface> list) {
        this.list = list;
    }

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "obdelnik";
    }

    /**
     * Run the command
     */
    @Override
    public void execute() {
        System.out.print("Zadejte stranu a: ");

        Scanner scanner = new Scanner(System.in);

        int a;

        try {
            a = scanner.nextInt();

            if(a <= 0) {
                System.err.println("Zadali jste neplatne cislo. Zkuste to prosim znovu.");

                return;
            }
        } catch (InputMismatchException e) {
            System.err.println("Zadejte prosim cele cislo.");

            return;
        } catch (Exception e) {
            System.err.println(String.format("Nastala chyba, kontaktujte vyvojare s chybou %s", e.getMessage()));

            return;
        }

        System.out.print("Zadejte stranu b: ");

        int b;

        try {
            b = scanner.nextInt();

            if(b <= 0) {
                System.err.println("Zadali jste neplatne cislo. Zkuste to prosim znovu.");

                return;
            }
        } catch (InputMismatchException e) {
            System.err.println("Zadejte prosim cele cislo. Zkuste to prosim znovu.");

            return;
        } catch (Exception e) {
            System.err.println(String.format("Nastala chyba, kontaktujte vyvojare s chybou %s", e.getMessage()));

            return;
        }

        Rectangle rectangle = new Rectangle(a, b);

        list.add(rectangle);

        System.out.println("Obdelnik ulozen");
    }
}
