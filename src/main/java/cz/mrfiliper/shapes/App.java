package cz.mrfiliper.shapes;

import cz.mrfiliper.shapes.commands.*;
import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.ArrayList;
import java.util.Scanner;

public class App {

    /**
     * Run app, arguments are ignored
     * @param ignored
     */
    public static void main(String[] ignored)
    {
        ArrayList<CommandInterface> commands = new ArrayList<>();
        ArrayList<ShapeInterface> shapes = new ArrayList<>();

        commands.add(new SquareCommand(shapes));
        commands.add(new RectangleCommand(shapes));
        commands.add(new RingCommand(shapes));
        commands.add(new ContentCommand(shapes));
        commands.add(new CircuitCommand(shapes));
        commands.add(new DeleteCommand(shapes));
        commands.add(new ExitCommand());
        commands.add(new HelpCommand());

        Scanner scanner = new Scanner(System.in);

        while(true) {
            String text = scanner.next();

            CommandInterface commandInterface = new HelpCommand();

            for(CommandInterface command : commands) {
                if(command.name().equalsIgnoreCase(text)) {
                    commandInterface = command;
                }
            }

            commandInterface.execute();
        }
    }
}
