package cz.mrfiliper.shapes.commands;

public interface CommandInterface {

    String name();
    void execute();
}
