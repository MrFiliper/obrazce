package cz.mrfiliper.shapes.shapes;

/**
 * Class represents rectangle
 */
public class Rectangle implements ShapeInterface {

    private int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    /**
     * Get content of rectangle
     * @return content
     */
    @Override
    public double content() {
        return a * b;
    }

    /**
     * Get circuit of rectangle
     * @return content
     */
    @Override
    public double circuit() {
        return 2 * a + 2 * b;
    }
}
