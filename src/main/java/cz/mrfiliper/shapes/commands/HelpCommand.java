package cz.mrfiliper.shapes.commands;

/**
 * Class represents exit command 'konec'
 */
public class HelpCommand implements CommandInterface {

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "help";
    }

    /**
     * Exit program
     */
    @Override
    public void execute() {
        System.out.println("Dostupne prikazy: ctverec, obdelnik, kruh, obsah, obvod, smazat, konec");
    }
}
