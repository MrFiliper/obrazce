package cz.mrfiliper.shapes.commands;

import cz.mrfiliper.shapes.shapes.Ring;
import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Class represents ring command 'kruh'
 */
public class RingCommand implements CommandInterface {

    private List<ShapeInterface> list;

    public RingCommand(List<ShapeInterface> list) {
        this.list = list;
    }

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "kruh";
    }

    /**
     * Run the command
     */
    @Override
    public void execute() {
        System.out.print("Zadejte polomer: ");

        Scanner scanner = new Scanner(System.in);

        int p;

        try {
            p = scanner.nextInt();

            if(p <= 0) {
                System.err.println("Zadali jste neplatne cislo. Zkuste to prosim znovu.");

                return;
            }
        } catch (InputMismatchException e) {
            System.err.println("Zadejte prosim cislo cislo. Zkuste to prosim znovu.");

            return;
        } catch (Exception e) {
            System.err.println(String.format("Nastala chyba, kontaktujte vyvojare s chybou %s", e.getMessage()));

            return;
        }

        Ring ring = new Ring(p);

        list.add(ring);

        System.out.println("Kruh ulozen");
    }
}
