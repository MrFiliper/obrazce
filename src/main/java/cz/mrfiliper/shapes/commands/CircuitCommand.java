package cz.mrfiliper.shapes.commands;

import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.List;

/**
 * Class represents circuit command 'obvod'
 */
public class CircuitCommand implements CommandInterface {

    private List<ShapeInterface> list;

    public CircuitCommand(List<ShapeInterface> list) {
        this.list = list;
    }

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "obvod";
    }

    /**
     * Get the circuit of all stored shape objects
     */
    @Override
    public void execute() {
        double obvod = 0;

        for(ShapeInterface o : list) {
            obvod += o.circuit();
        }

        if(obvod % 1 == 0) {
            System.out.println(String.format("Obvod je: %d", (int) obvod));
        } else {
            System.out.println(String.format("Obvod je: %f", obvod));
        }
    }
}