package cz.mrfiliper.shapes.commands;

/**
 * Class represents exit command 'konec'
 */
public class ExitCommand implements CommandInterface {

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "konec";
    }

    /**
     * Exit program
     */
    @Override
    public void execute() {
        System.exit(0);
    }
}
