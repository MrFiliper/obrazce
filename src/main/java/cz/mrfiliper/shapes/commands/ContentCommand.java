package cz.mrfiliper.shapes.commands;

import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.List;

/**
 * Class represents content command 'obsah'
 */
public class ContentCommand implements CommandInterface {

    private List<ShapeInterface> list;

    public ContentCommand(List<ShapeInterface> list) {
        this.list = list;
    }

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "obsah";
    }

    /**
     * Get the content of all stored shape objects
     */
    @Override
    public void execute() {
        double obsah = 0;

        for(ShapeInterface o : list) {
            obsah += o.content();
        }

        if(obsah % 1 == 0) {
            System.out.println(String.format("Obsah je: %d", (int) obsah));
        } else {
            System.out.println(String.format("Obsah je: %f", obsah));
        }
    }
}
