package cz.mrfiliper.shapes.shapes;

public interface ShapeInterface {

    double content();
    double circuit();
}
