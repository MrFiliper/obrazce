package cz.mrfiliper.shapes.shapes;

/**
 * Class represents square
 */
public class Square implements ShapeInterface {

    private int a;

    public Square(int a) {
        this.a = a;
    }

    /**
     * Get content of square
     * @return content
     */
    @Override
    public double content() {
        return a * a;
    }

    /**
     * Get circuit of square
     * @return circuit
     */
    @Override
    public double circuit() {
        return 4 * a;
    }
}
