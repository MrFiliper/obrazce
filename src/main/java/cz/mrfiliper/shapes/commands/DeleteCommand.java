package cz.mrfiliper.shapes.commands;

import cz.mrfiliper.shapes.shapes.ShapeInterface;

import java.util.List;

/**
 * Class represents delete command 'smazat'
 */
public class DeleteCommand implements CommandInterface {

    private List<ShapeInterface> list;

    public DeleteCommand(List<ShapeInterface> list) {
        this.list = list;
    }

    /**
     * Return the name of the command
     * @return name of command
     */
    @Override
    public String name() {
        return "smazat";
    }

    /**
     * Clear all shape objects
     */
    @Override
    public void execute() {
        list.clear();

        System.out.println("Tvary smazany");
    }
}
