package cz.mrfiliper.shapes.shapes;

/**
 * Class represents ring
 */
public class Ring implements ShapeInterface {

    private int p;

    public Ring(int p) {
        this.p = p;
    }

    /**
     * Get content of ring
     * @return content
     */
    @Override
    public double content() {
        return Math.PI * Math.pow(p, 2);
    }

    /**
     * Get circuit of ring
     * @return circuit
     */
    @Override
    public double circuit() {
        return 2 * Math.PI * p;
    }
}
